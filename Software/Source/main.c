/* 
 * File:   main.c
 * Author: nickhudspeth
 *
 * Created on September 8, 2013, 4:32 PM
 */

#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__dsPIC33E__)
    	#include <p33Exxxx.h>
    #elif defined(__dsPIC33F__)
    	#include <p33Fxxxx.h>
    #endif
#endif

//#define F_OSC 40000000              /* CPU frequency of selected oscillator (FRC,XT,LP,LPRC)*/
//#define F_CPU F_OSC/2               /* Actual instruction clock speed */
//#define PLL 1                       /* PLL clock multiplier: 1(NO PLL) 2, 4, 8,*/
//
//#define CCPI 4                      /* Clock cycles per instruction */
//#define FCY  F_CPU*PLL/CCPI
//#define MIPS FCY/1000000            /* Instruction speed (MIPS) */
#define FCY 40000000
#define BAUDRATE 9600
#define BRGVAL ((FCY/BAUDRATE)/16) - 1

#define CCD_DUMMY_PREFIX_LENGTH 33  /* 33 pixels dummy padding at beginning of CCD data stream */
#define CCD_DUMMY_SUFFIX_LENGTH 6   /* 6 pixels dummy padding at end  of CCD data stream */
#define CCD_DATA_LENGTH 2048        /* 2048 pixels CCD effective picture elements signal */

#define INTEGRATION_TIME 10         /* CCD integration time, in milliseconds */
#define ADC_SAMPLE_OFFSET 0         /* Offset from rising CCD_SIG_IN pulse edge*/

#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include <libpic30.h>               /* Included for delay functions. */
#include "system.h"
#include "config.h"

/*
 * 
 */


//int ccdData[CCD_DATA_LENGTH];
unsigned int* ccdData;

volatile int ACCF = 0; /* ADC conversion complete flag; set by INT0 interrupt routine.*/

int main(int argc, char** argv) {

  //  Setup();

    while(1) {
     //do stuff   ;
        CCDGetData();
    }

    return (EXIT_SUCCESS);
}


void Setup() {
    SetupOscillator();
    SetupGPIO();
 //   SetupTimers();
    SetupDMA();
 //   SetupADC();
 //   SetupUART();
//    SetupGlobalInterrupts();
 //   SetupPinChangeInterrupt();
}
  


void CCDGetData () {
    int i;
    CP_OUT = 1;         /* Write CCD data clock line high. */
    ROG_OUT = 0;        /* Write ROG pin low. */
    __delay_us(1);        /* Keep ROG pin low for 1 us. */
    ROG_OUT = 1;        /* Write ROG pin high. */

    __delay_ms(INTEGRATION_TIME);
    for(i=0; i<CCD_DUMMY_PREFIX_LENGTH; i++) {

        CP_OUT = 0;     /*Write clock pulse pin low*/
        CP_OUT = 1;     /*Write clock pulse pin high*/
    }

    for(i=0;i<CCD_DATA_LENGTH;i++) {
        
        CP_OUT = 0;     /* Write clock pulse pin low. */
        CP_OUT = 1;     /* Write clock pulse pin high. */
        
       // while(!ACCF);   /* Wait for the interrupt to fire and set the ADC
       //                  * conversion complete flag. */
     //   while(!_DONE);  /* Wait for the interrupt to fire and set the ADC
    //                     * conversion complete register.*/
      //  ccdData[i] = ;
    //    ACCF = 0;       /* Clear ADC conversion complete flag.*/

    }

    for(i=0;i<CCD_DUMMY_SUFFIX_LENGTH;i++) {
        CP_OUT = 0;     /* Write clock pulse pin low. */
        CP_OUT = 1;     /* Write clock pulse pin high. */
    }



}

void __attribute__((interrupt,no_auto_psv)) _INT0Interrupt(void) {

    _SAMP = 1;      /* Trigger ADC conversion. */
  //  while(!_DONE);  /* Wait for the ADC conversion to complete. */
  //  ACCF = 1;       /* Set ADC conversion complete flag. */
    
}

void __attribute__((interrupt, no_auto_psv)) _DMA0Interrupt(void) {
    _DMA0IF=0;  /* Clear DMA0 interrupt flag.*/

}

void __attribute__((interrupt, no_auto_psv)) _DMA1Interrupt(void) {
    _DMA1IF=0;  /* Clear DMA1 interrupt flag.*/
}