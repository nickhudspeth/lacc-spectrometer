/* 
 * File:   config.h
 * Author: nickhudspeth
 *
 * Created on September 8, 2013, 11:17 PM
 */

#ifndef CONFIG_H
#define	CONFIG_H

void SetupOscillator(void);
void SetupGPIO(void);
void SetupTimers(void);
void SetupDMA(void);
void SetupADC(void);
void SetupUART(void);
void SetupGlobalInterrupts(void);
void SetupPinChangeInterrupt(void);




#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* CONFIG_H */

