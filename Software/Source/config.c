/*
 * File:   config.c
 * Author: nickhudspeth
 *
 * Created on September 8, 2013, 4:32 PM
 */

/* Device header file */
#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__dsPIC33E__)
    	#include <p33Exxxx.h>
    #elif defined(__dsPIC33F__)
    	#include <p33Fxxxx.h>
    #endif
#endif

_FOSCSEL(FNOSC_FRC) /* Set clock source to internal oscillator w/ PLL. */
_FOSC(FCKSM_CSECMD & OSCIOFNC_OFF & POSCMD_NONE)   /* Disable clock output on OSC2 pin,
                                                    * disable external oscillator. */
_FWDT(FWDTEN_OFF)   /* Disable watchdog timer. */

/* Sets up the main system clock, as suggested in Section 39. Oscillator (Part III)
   Oscillator is configured for 40MIPS operation. */

void SetupOscillator() {
    _PLLDIV = 41;   /* M = 43 */
    _PLLPRE = 0;    /* N2 = 2 */
    _PLLPOST = 0;   /* N1 - 2 */
    
    OSCCONH = 0x03;
    OSCCONL = 0x01;

    while(_COSC != 0b011); /* Wait for osc switch to occur. */
    while(!_LOCK); /* Wait for PLL to lock. */

}

void SetupGPIO() {
    _TRISB12 = 0;  /*Configure ROG_OUT AS OUTPUT*/
    _TRISB13 = 0;  /*Configure CP_OUT AS OUTPUT*/
    _TRISB0 = 1;   /*Configure CCD_SIG_IN AS INPUT*/

}


/* Sets up all system timers*/
void SetupTimers() {


}

/* Sets up and configures two DMA channels:
    1. ADC->DMA conversion result transfer.
    2. DMA->UART TX byte transfer. */
void SetupDMA() {
    DMA0REQbits.IRQSEL = 13;    /* Associate DMA Channel 0 with ADC1BUFF0. */
    DMA1REQbits.IRQSEL = 12;    /* Associate DMA Channel 1 with UART1TX*/
    DMA0STA = 0;                /* Set DMA Channel 0 memory address offset to 0. */
    DMA1STA = 0;
    DMA0CNT = 511;              /* 512 words must be transferred before a block
                                 * transfer is considered complete.*/
    DMA1CNT = 511;
    DMA0CONbits.SIZE = 1;       /* Set DMA Channel 0 to transfer data by byte.*/
    DMA1CONbits.SIZE = 1;
    DMA0CONbits.DIR = 0;        /* Set DMA Channel 0 to read from peripheral. */
    DMA1CONbits.DIR = 1;        /* Set DMA Channel 1 to write to peripheral. */
    DMA0CONbits.HALF = 0;       /* Initiate interrupt when all of data has been moved.*/
    DMA1CONbits.HALF = 0;
    DMA0CONbits.AMODE = 0;      /* Set channel operating mode to increment DMA
                                 * pointer automatically after each R/W. */
    DMA1CONbits.AMODE = 0;
    DMA0CONbits.MODE = 0;       /* Set DMA Channel 0 to continuous mode.*/
    _DMA0IE = 1;                /* Enable DMA Channel 0 transfer complete interrupt. */
    _DMA1IE = 1;                /* Enable DMA Channel 1 transfer complete interrupt. */
    _DMA0IF = 0;                /* Clear DMA0 interrupt flag. */
    _DMA1IF = 0;                /* Clear DMA0 interrupt flag. */
    DMA0CONbits.CHEN = 1;       /* Enable DMA Channel 0. */
    DMA1CONbits.CHEN = 1;       /* Enable DMA Channel 1. */

}

/*  ADC initialization routine. See "ADC Initialization section on page 288 in
    DSPIC33FJ64MC802 for more information. */
void SetupADC() {
    _AD12B = 1;     /* 1. Configure ADC for 12-bit conversion */
    _VCFG = 0;      /* 2. Set ADREF+- sources to AVDD and AVSS. (5V R2R) */
    _ADRC = 0;      /* Derive ADC clock from system clock*/
    _ADCS = 0;      /* Set ADCS to 1 - TCY=TAD*/
    _PCFG2 = 0;     /* 4.  ADC pin AN2/RB0 as input */

    _SSRC = 3;      /* Set ADC to auto-convert. Sample time is automatically
                     * regulated by the internal ADC counter. */
    _ASAM = 0;      /* 8. Set ADC to begin sampling when SAMP bit is set. */
    
    _FORM = 0;      /* 10. Set ADC to output results as unsigned integer */
    
    

    
    _SMPI = 0;      /* Set ADC to increment DMA buffer pointer after each
                     * conversion.*/
    _DMABL = 0;     /* Allocate 1 word of DMA buffer to each analog input*/
    _ADDMABM = 1;   /* Set ADC to generate ADC addresses in order of conversion. */

    _ADON = 1;      /* 16. Turn on ADC */


}

void SetupUART() {
    _STSEL = 0;     /* 1 stop bit. */
    _PDSEL = 0;     /* No parity, 8 data bits. */
    _ABAUD = 0;     /* Disable auto baud. */
    _UTXISEL0 = 0;  /* Interrupt after one TX xharacter is transmitted. */
    _UTXISEL1 = 0;
    _URXISEL = 0;   /* Interrupt after one RX character is received. */
    _UARTEN = 1;    /* Enable UART. */
    _UTXEN = 1;     /* Enable UART TX. */
    _U2EIF = 0;     /* Clear UART2 error interrupt flag. */
    _U2EIE = 1;     /* Enable UART2 error interrupt. */
}

void SetupGlobalInterrupts() {
    /* INTERRUPT NESTING IS ENABLED BY DEFAULT!*/
    _NSTDIS = 0;        /* Enable interrupt nesting globally. */
    SET_CPU_IPL(0);     /* Set CPU interrupt priority level to zero.
                           All interrupts are triggered above this level.*/

}

void SetupPinChangeInterrupt() {
    _INT0IP = 6; /* Set interrupt priority to 6 (out of 7)*/
    _INT0IF = 0; /* Clear the interrupt flag status bit for INT0. */
    _INT0EP = 0; /* Enable interrupt on positive edge for INT0.
                  * INT0 is CCD_SIG_TRIG_IN, the analog signal trigger in from CCD;
                  * This is a split trace from CCD_SIG_IN, the Analog signal in from CCD.
                  * On the positive rising edges of the CCD in signal, the interrupt is
                  * fired, triggering a reading of the signal level from the ADC. */
    _INT0IE = 1; /* Enable interrupt on INT0. */
}

