/* 
 * File:   system.h
 * Author: nickhudspeth
 *
 * Created on September 8, 2013, 10:16 PM
 */

#ifndef SYSTEM_H
#define	SYSTEM_H

#ifdef	__cplusplus
extern "C" {
#endif


/* Device header file */
#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__dsPIC33E__)
    	#include <p33Exxxx.h>
    #elif defined(__dsPIC33F__)
    	#include <p33Fxxxx.h>
    #endif
#endif




#define ROG_OUT _RB12          /* ROG pulse output for CCD  ~ACTIVE LOW!~ */
#define CP_OUT  _RB13          /* Clock pulse output for CCD*/
#define CCD_SIG_IN _RB0        /* Analog signal in from CCD */
#define CCD_SIG_TRIG_IN _RB7   /* Analog signal trigger in from CCD*/






#ifdef	__cplusplus
}
#endif

#endif	/* SYSTEM_H */

